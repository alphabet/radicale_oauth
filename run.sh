#!/bin/bash

rm -f oauth2.tar.gz && tar -czf oauth2.tar.gz oauth2
docker-compose down
docker-compose up --build -d
docker-compose logs -f radicale
