# radicale_oauth

docker installation of radicale with a custom oauth2 authentication plugin

based on tomsquest's docker image (https://hub.docker.com/r/tomsquest/docker-radicale/)

# testing with keycloak

- create a client in keycloak server (see json file in keycloak folder)
- edit token endpoint in config file
- launch run.sh script to generate containers
- try to authenticate with your keycloak account on http://localhost:5232

## Contributing to this project

### Commit messages

Commit messages formatting is **significant**!

Please, see [How to contribute](docs/CONTRIBUTING.md) for more details
