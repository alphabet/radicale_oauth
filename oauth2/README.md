# Oauth2 authentication plugin for Radicale

[Radicale] is a CalDAV and CardDAV server, for storing calendars and
contacts.  This python module provides an authentication plugin for Radicale
that authenticates to the authorization endpoint of an oauth2 server

[Radicale]: https://radicale.org/

## Installation

```shell
pip3 install radicale-auth-oauth2
```

## Configuration

```in Radicale's INI
[auth]
# Authentication method
type = radicale_auth_oauth2
# PAM Service used for authentication
oauth2_auth_endpoint = <oauth2 authentication endpoint>
```
