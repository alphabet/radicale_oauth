#!/usr/bin/env python3

import setuptools

with open("README.md", "r") as fh:
    desc = fh.read()

setuptools.setup(
    name="radicale_auth_oauth2",
    description="Oauth2 authentication plugin for Radicale",
    long_description=desc,
    author="Bruno Boiget",
    author_email="bruno.boiget@ac-dijon.fr",
    url="https://gitlab.com/eole/radicale_auth_oauth2",
    install_requires=[
        "requests",
    ],
    packages=setuptools.find_packages(),
    include_package_data=True,
    license="GPL3+",
    classifiers=[
        "Development Status :: 1 - Beta",
        "Environment :: Plugins ",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Security",
        "Topic :: System :: Systems Administration :: Authentication/Directory",
    ],
)
